""""
Nume: Costea Adrian-Ioan
Grupa: 1, An 2
_____________________________________________________________________________________________________________
App that acceses a link given from config file and prints the title of the html web page and description meta
*********************************config file structure*******************************************************
[database]
URL = https://docs.python.org/3/library/configparser.html#supported-ini-file-structure
*************************************************************************************************************
_____________________________________________________________________________________________________________
"""

#Library imports
import requests
from bs4 import BeautifulSoup
from configparser import ConfigParser

def main():
    # getting the url from the config file
    print('Reading URL from config file')

    config = ConfigParser()
    config.read("config.ini")

    url = config['database']['URL']

    # making requests instance
    reqs = requests.get(url)

    # using the BeaitifulSoup module
    soup = BeautifulSoup(reqs.text, 'html.parser')

    # displaying the title
    print('The title of the website is: ')
    for title in soup.find_all('title'):
        print(title.get_text())

    # extracting meta description
    metas = soup.find_all('meta')

    #printing meta description
    for meta in metas:
        if 'name' in meta.attrs and meta.attrs['name'] == 'description':
            print(meta.attrs['content'])
if __name__ == "__main__":
    main()
